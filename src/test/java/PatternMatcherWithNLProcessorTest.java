package test.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import frazeusz.patternMatcher.PatternMatcher;
import frazeusz.patternMatcher.Phrase;
import search.NLProcessor;
import search.WordVariant;

import static org.junit.Assert.*;

public class PatternMatcherWithNLProcessorTest {

    static NLProcessor nlp;

    @BeforeClass
    public static void oneTimeSetUp() throws Exception{
        nlp = new NLProcessor();
        //nlp.loadDictionaries();
    }

    @Test
    public void testPatternMatcherNLProcessorSimple(){
        Phrase phrase = new Phrase("kotki", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("Dwa kotki. Jedna kotka.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(2), results.get(phrase));
    }

    @Test
    public void testPatternMatcherNLProcessorMultipleWordResult(){
        Phrase phrase = new Phrase("bonanza", new WordVariant(false, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("dojna krowa. kokosowy interes");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(2), results.get(phrase));
    }

    @Test
    public void testExpression(){
        Phrase phrase = new Phrase("proszek do pieczenia", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("proszek do pieczenia");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
    }

    @Test
    public void testExpression2(){
        Phrase phrase = new Phrase("proszku do pieczenia", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("proszek do pieczenia");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
    }

    @Test
    public void testExpressionMarked(){
        Phrase phrase = new Phrase("(proszek do pieczenia)", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("proszku do pieczenia");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
    }

    @Test
    public void testExpressionUnmarked(){
        Phrase phrase = new Phrase("dojna krowa", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("bonanza");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(0), results.get(phrase));
    }

    @Test
    public void testDifferenceBetweenMarkedAndUnmarked(){
        Phrase phraseUnmarked = new Phrase("dojna krowa", new WordVariant(false, true, false));
        Phrase phraseMarked = new Phrase("(dojna krowa)", new WordVariant(false, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phraseMarked);
        phrases.add(phraseUnmarked);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("bonanza");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertEquals(new Integer(0), results.get(phraseUnmarked));
        assertEquals(new Integer(1), results.get(phraseMarked));
    }

    @Test
    public void testExpressionMarkedFound(){
        Phrase phrase = new Phrase("(dojna krowa)", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();
        text.add("bonanza");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(1), results.get(phrase));
    }

    @Test
    public void testBigResultSet(){
        Phrase phrase = new Phrase("(prymitywny guz neuroektodermalny)", new WordVariant(true, true, false));
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, nlp);
        List<String> text = new ArrayList<String>();

        BufferedReader br = null;
        FileReader fr = null;
        try{
            fr = new FileReader("src/test/resources/lorem_ipsum.txt");
            br = new BufferedReader(fr);
            String line;
            while((line = br.readLine()) != null){
                text.add(line);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(0), results.get(phrase));
    }

}
