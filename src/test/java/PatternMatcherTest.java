package test.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import frazeusz.patternMatcher.PatternMatcher;
import frazeusz.patternMatcher.Phrase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PatternMatcherTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testPatternMatcherConstructor(){
        Phrase phrase = new Phrase("lorem ipsum", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        assertNotNull(patternMatcher);
        assertEquals(0, patternMatcher.getBytes());
    }

    @Test
    public void testGetBytes(){
        Phrase phrase = new Phrase("lorem ipsum", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet.");
        patternMatcher.searchText(text);
        assertEquals(27, patternMatcher.getBytes());
    }

    @Test
    public void testGetBytesNone(){
        PatternMatcher patternMatcher = new PatternMatcher(null, null);
        assertEquals(0, patternMatcher.getBytes());
        List<String> text = new ArrayList<String>();
        text.add("");
        patternMatcher.searchText(text);
        assertEquals(0, patternMatcher.getBytes());
    }

    @Test
    public void testGetResults(){
        Phrase phrase = new Phrase("Lorem ipsum", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(1), results.get(phrase));
    }

    @Test
    public void testGetResultsZero(){
        Phrase phrase = new Phrase("Ala ma kota", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(0), results.get(phrase));
    }

    @Test
    public void testFindComplexPhrase(){
        Phrase phrase = new Phrase("Ala [2] kota [4] psa", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Ala nie ma kota ale za to ma psa");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.containsKey(phrase));
        assertEquals(new Integer(1), results.get(phrase));
    }

    @Test
    public void testGetResultsMultiple(){
        Phrase phrase = new Phrase("Lorem ipsum", null);
        Phrase phrase2 = new Phrase("dolor [2] amet", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        phrases.add(phrase2);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet. Lorem ipsum ipsum amet.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertEquals(2, results.size());
        assertEquals(new Integer(2), results.get(phrase));
        assertEquals(new Integer(1), results.get(phrase2));
    }

    @Test
    public void testGetResultsMultipleTimes(){
        Phrase phrase = new Phrase("Lorem ipsum", null);
        Phrase phrase2 = new Phrase("dolor [2] amet", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        phrases.add(phrase2);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet. Lorem ipsum ipsum amet.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertEquals(2, results.size());
        assertEquals(new Integer(2), results.get(phrase));
        assertEquals(new Integer(1), results.get(phrase2));

        patternMatcher.searchText(text);
        results = patternMatcher.getResults();
        assertEquals(2, results.size());
        assertEquals(new Integer(4), results.get(phrase));
        assertEquals(new Integer(2), results.get(phrase2));
    }

    @Test
    public void testDuplicatePhrase(){
        Phrase phrase = new Phrase("Lorem ipsum", null);
        Phrase phrase2 = new Phrase("Lorem ipsum", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        phrases.add(phrase2);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertEquals(2, results.size());
        assertEquals(new Integer(1), results.get(phrase));
        assertEquals(new Integer(1), results.get(phrase2));
    }

    @Test
    public void testEmptyPhrase(){
        Phrase phrase = new Phrase("", null);
        Phrase phrase2 = new Phrase("", null);
        Phrase phrase3 = new Phrase("Ala", null);
        List<Phrase> phrases = new ArrayList<Phrase>();
        phrases.add(phrase);
        phrases.add(phrase2);
        phrases.add(phrase3);
        PatternMatcher patternMatcher = new PatternMatcher(phrases, null);
        List<String> text = new ArrayList<String>();
        text.add("Lorem ipsum dolor sit amet.");
        patternMatcher.searchText(text);
        Map<Phrase, Integer> results = patternMatcher.getResults();
        assertEquals(1, results.size());
    }
}