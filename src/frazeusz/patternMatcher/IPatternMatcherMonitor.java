package frazeusz.patternMatcher;

public interface IPatternMatcherMonitor {
    public long getBytes();
}
