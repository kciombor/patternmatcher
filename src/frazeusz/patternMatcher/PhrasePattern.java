package frazeusz.patternMatcher;

import java.util.regex.Pattern;

public class PhrasePattern {
    private Phrase phrase;
    private Pattern pattern;

    PhrasePattern(Phrase phrase, Pattern pattern){
        this.phrase = phrase;
        this.pattern = pattern;
    }

    public  Phrase getPhrase(){
        return this.phrase;
    }

    public Pattern getPattern() {
        return pattern;
    }
}
