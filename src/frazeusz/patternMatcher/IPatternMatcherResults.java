package frazeusz.patternMatcher;

import java.util.Map;

public interface IPatternMatcherResults {
    public Map<Phrase, Integer> getResults();
}
