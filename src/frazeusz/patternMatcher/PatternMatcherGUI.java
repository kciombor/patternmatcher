package frazeusz.patternMatcher;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class PatternMatcherGUI extends JPanel {
	
	private static final long serialVersionUID = 1L;

	private JPanel header;
	private JPanel main;
	private JPanel bottom;
	
	public PatternMatcherGUI() {
		super(new BorderLayout());
		
		header = new JPanel();
		header.add(new JLabel("Wprowadzanie frazy do wyszukania"));
		
		main = new JPanel();
		main.setLayout(new GridLayout(0, 1));
		main.add(new PhrasePanel());
		main.add(Box.createVerticalGlue(), -1);
		
		JScrollPane scroll = new JScrollPane(main);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		bottom = new JPanel();
		JButton dodaj = new JButton("+ dodaj frazę");
		dodaj.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				main.add(new PhrasePanel(), 0);
				main.revalidate();
				main.repaint();
			}
		});
		bottom.add(dodaj);
		
		add(header, BorderLayout.PAGE_START);
		add(scroll, BorderLayout.CENTER);
		add(bottom, BorderLayout.PAGE_END);
	}
	
	public List<Phrase> getPhrases() {
		ArrayList<Phrase> phrases = new ArrayList<Phrase>();
		for(Component phrasePanel : main.getComponents()){
			phrases.add(((PhrasePanel) phrasePanel).getPhrase());
		}
		return phrases;
	}
	
	public void validate() {}
}
