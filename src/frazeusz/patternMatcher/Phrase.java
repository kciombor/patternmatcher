package frazeusz.patternMatcher;

import search.WordVariant;

public class Phrase {
    private String text;
    private WordVariant wordVariant;

    public Phrase(String text, WordVariant wordVariant) {
        this.text = text;
        this.wordVariant = wordVariant;
    }

    public String getText() {
        return this.text;
    }

    @Override
    public String toString() {
        return this.text;
    }

    public WordVariant getWordVariant() {
        return this.wordVariant;
    }
}
