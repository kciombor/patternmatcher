package frazeusz.patternMatcher;

import search.WordVariant;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PhrasePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	JTextField fraza;
	JPanel checkboksy;
	JCheckBox synonimy;
	JCheckBox odmiany;
	JCheckBox zdrobnienia;

	public PhrasePanel() {
		super(new BorderLayout());
		
		JPanel header = new JPanel(new FlowLayout());
		
		header.add(new JLabel("szukana fraza"));
		
		fraza = new JTextField(20);
		fraza.setMinimumSize(new Dimension(1000, 10));
		header.add(fraza);
		
		/*BufferedImage halpIcon = null;
		try {
			halpIcon = ImageIO.read(new File("helpicon.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JLabel halpLabel = new JLabel(new ImageIcon(halpIcon));
		header.add(halpLabel);*/
		
		add(header, BorderLayout.PAGE_START);
	
		
		checkboksy = new JPanel();
		synonimy = new JCheckBox("synonimy");
		checkboksy.add(synonimy);
		odmiany = new JCheckBox("odmiany");
		checkboksy.add(odmiany);
		zdrobnienia = new JCheckBox("zdrobnienia");
		checkboksy.add(zdrobnienia);
		add(checkboksy, BorderLayout.WEST);
		
		/*BufferedImage xIcon = null;
		try {
			xIcon = ImageIO.read(new File("xicon.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JLabel xLabel = new JLabel(new ImageIcon(xIcon));
		add(xLabel, BorderLayout.EAST);*/
	}

	public Phrase getPhrase(){
		return new Phrase(fraza.getText(), new WordVariant(synonimy.isSelected(), odmiany.isSelected(), zdrobnienia.isSelected()));
	}
}
