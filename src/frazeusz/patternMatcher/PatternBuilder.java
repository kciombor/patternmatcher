package frazeusz.patternMatcher;

import search.NLProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class PatternBuilder {

    private static List<String> splitParts(String input){
        List<String> parts = new ArrayList<String>();
        boolean expression = false;
        int startMarker = 0;
        boolean wasWhitespace = false;
        char expressionStartMarker = '(';
        char expressionStopMarker = ')';

        for (int i = 0; i < input.length(); i++) {
            if(Character.isWhitespace(input.charAt(i))){
                if(!expression){
                    parts.add(input.substring(startMarker, i).replace("(", "").replace(")", ""));
                    wasWhitespace = true;
                }
                else if(wasWhitespace){
                    continue;
                }
            }
            else{
                if(wasWhitespace) {
                    wasWhitespace = false;
                    startMarker = i;
                }
                if(input.charAt(i) == expressionStartMarker){
                    expression = true;
                }
                if(input.charAt(i) == expressionStopMarker){
                    expression = false;
                }
            }
        }
        parts.add(input.substring(startMarker, input.length()).replace("(", "").replace(")", ""));

        return parts;
    }

    public static Pattern buildPattern(Phrase in, NLProcessor nlp) {
        String delimiters = "\\s+";
        //String[] parts = in.getText().split(delimiters);
        List<String> parts = splitParts(in.getText());

        String patternString = "";
        for(String part: parts){
            if(part.matches("\\[[0-9]+\\]")){
                String maxWordsSpace = part.substring(1, part.length() - 1);
                patternString += "(?:\\w+\\W+){0,"+ maxWordsSpace +"}?";
            }
            else{
                if(nlp != null) {
                    Set<String> processedWords = nlp.findWords(part, in.getWordVariant());
                    patternString += "(";
                    for(String processedWord : processedWords){
                        patternString += processedWord.trim() + "|"; //necessary to omit leading and trailing whitespaces
                    }
                    patternString = patternString.substring(0, patternString.length() - 1);
                    patternString += ")\\s*";
                }
                else{
                    patternString += part + "\\s*";
                }
            }
        }
        return Pattern.compile(patternString);
    }
}
