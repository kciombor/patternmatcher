package frazeusz.patternMatcher;

import search.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

public class PatternMatcher implements IPatternMatcherMonitor, IPatternMatcherParser, IPatternMatcherResults {
    private List<PhrasePattern> patterns;
    private Map<Phrase, Integer> results;
    private long bytesCount;

    /**
     * Constructor for PatternMatcher
     * @param phrases List of Phrases to search. Either from PatternMatcherGUI or outside source like from file
     * @param nlp Instance of NLProcessor, Dependency Injection done by MainGUI
     */
    public PatternMatcher(List<Phrase> phrases, NLProcessor nlp) {
        this.patterns = new ArrayList<PhrasePattern>();
        this.results = new ConcurrentHashMap<Phrase, Integer>();
        this.bytesCount = 0;

        if(phrases != null) {
            for (Phrase phrase : phrases) {
                if(!phrase.getText().equals("")) {
                    patterns.add(new PhrasePattern(phrase, PatternBuilder.buildPattern(phrase, nlp)));
                    results.put(phrase, 0);
                }
            }
        }
    }

    /**
     * Returns current count of processed bytes
     * @return bytesCount
     */
    @Override
    public long getBytes() {
        long bytesCountCopy;
        synchronized (this) {
            bytesCountCopy = this.bytesCount;
        }
        return bytesCountCopy;
    }

    /**
     * Used by Parser to put text for PatternMatcher to process
     * @param text List containing blocks of text to process
     */
    @Override
    public void searchText(List<String> text) {
        for(String textBlock : text) {
            for (PhrasePattern pattern : patterns) {
                Matcher matcher = pattern.getPattern().matcher(textBlock);
                while(matcher.find()){
                    Phrase key = pattern.getPhrase();
                    results.put(key, results.get(key) + 1);
                }
            }
            synchronized (this){
                bytesCount += textBlock.length();
            }
        }
    }

    /**
     * Used by Plotter and Writer to get current results
     * @return Map containing Phrases and current occurrence count
     */
    @Override
    public Map<Phrase, Integer> getResults() {
        return new HashMap<Phrase, Integer>(results);
        //return this.results; //returning reference to ConcurrentHashMap, which is thread-safe
    }
}
